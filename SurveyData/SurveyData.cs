﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Survey
{
    public class SurveyData
    {
        public IList<Question> Questions { get; set; }
        public IList<Entry> Entries { get; set; }

        public SurveyData()
        {
            Questions = new List<Question>();
            Entries = new List<Entry>();
        }

        public SurveyData(IEnumerable<Question> questions)
        {
            Questions = questions.ToList();
        }
    }

    public class Question
    {
        public string Number { get; set; }
        public string Title { get; set; }
        public bool IsOpenEnded { get; set; }

        public Question(string number, string title, bool isOpenEnded=false)
        {
            Number = number;
            Title = title;
            IsOpenEnded = isOpenEnded;
        }
    }

    public class Entry
    {
        public IList<Answer> Answers { get; set; }

        public Entry(IList<Answer> answers)
        {
            Answers = new List<Answer>();

            foreach (var answer in answers)
                Answers.Add(answer);
        }
    }

    public class Answer
    {
        public string Value { get; set; }

        public Answer(string value)
        {
            Value = value;
        }
    }
}
