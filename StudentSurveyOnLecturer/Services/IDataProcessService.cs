﻿namespace StudentSurveyOnLecturer.Services
{
    public interface IDataProcessService
    {
        void OnQuit();
        void Start();
    }
}