﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StudentSurveyOnLecturer.Services
{
    public class ImportService
    {
        OpenFileDialog fileDialog;

        public string GetDataLocation()
        {
            fileDialog = new OpenFileDialog();
            fileDialog.Filter = "csv files (*.csv)|*.csv";

            if (fileDialog.ShowDialog() == DialogResult.OK)
                return fileDialog.FileName;

            return string.Empty;
        }

        public string GetTemplateLocation()
        {
            fileDialog = new OpenFileDialog();
            fileDialog.Filter = "dotx files (*.dotx)|*.dotx";

            if (fileDialog.ShowDialog() == DialogResult.OK)
                return fileDialog.FileName;

            return string.Empty;
        }
    }
}
