﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualBasic.FileIO;
using StudentSurveyOnLecturer.Models;
using Word = Microsoft.Office.Interop.Word;
using System.IO;

namespace StudentSurveyOnLecturer.Services
{
    public class DataProcessService
    {
        string pathToSurveyData;
        string pathToReportData;
        object pathToReportTemplate;
        string pathToExport;
        IList<SurveyEntry> surveyEntries;
        IList<SurveyReport> surveyReports;
        Word.Application app;
        Word.Document doc;

        public EventHandler<string> update;               

        public DataProcessService(string pathToSurveyData, string pathToReportData, string pathToReportTemplate, string pathToExport)
        {
            this.pathToSurveyData = pathToSurveyData;
            this.pathToReportData = pathToReportData;
            this.pathToReportTemplate = pathToReportTemplate;
            this.pathToExport = pathToExport;
        }

        public void Start()
        {
            ReadSurveyData();
            ReadReportData();
            GenerateIndividualReport();
            GenerateSummaryReport();
        }        

        private void ReadSurveyData()
        {
            surveyEntries = new List<SurveyEntry>();

            using (var parser = new TextFieldParser(pathToSurveyData))
            {
                parser.Delimiters = new string[] { "," };
                parser.HasFieldsEnclosedInQuotes = true;

                string[] headers = parser.ReadFields();

                while (!parser.EndOfData)
                {
                    string[] row = parser.ReadFields();

                    surveyEntries.Add(new SurveyEntry(row));
                }
            }

            update?.Invoke(this, $"reading of survey data has been completed.");
        }

        private void ReadReportData()
        {
            surveyReports = new List<SurveyReport>();

            using (TextFieldParser parser = new TextFieldParser(pathToReportData))
            {
                parser.Delimiters = new string[] { "," };
                parser.HasFieldsEnclosedInQuotes = true;

                string[] headers = parser.ReadFields();

                while (!parser.EndOfData)
                {
                    string[] columns = parser.ReadFields();

                    surveyReports.Add(new SurveyReport(columns));
                }
            }
            update?.Invoke(this, $"reading of report data has been completed.");
        }

        private void GenerateIndividualReport()
        {
            app = new Word.Application();

            var locationForIndividualReports = Path.Combine(pathToExport, "individual reports");
            if (!Directory.Exists(locationForIndividualReports))
                Directory.CreateDirectory(locationForIndividualReports);
            else
            {
                DirectoryInfo directory = new DirectoryInfo(locationForIndividualReports);

                foreach (FileInfo file in directory.EnumerateFiles())
                {
                    file.Delete();
                }
            }

            foreach (SurveyReport report in surveyReports)
            {
                doc = app.Documents.Add(ref pathToReportTemplate);

                var results = surveyEntries.Where(result => result.Id == report.Id).ToList();

                ReplaceText("[StudyTerm]", report.StudyTerm);
                ReplaceText("[LecturerName]", report.Lecturer);
                ReplaceText("[Unit]", $"{report.UnitCode.ToString()} - {report.UnitName}");
                ReplaceText("[Population]", report.ClassSize.ToString());
                ReplaceText("[Response]", $"{results.Count().ToString()} ({GetPercent((double)results.Count() / report.ClassSize)})");

                for (int count = 1; count <= 10; count++)
                {
                    var SA = results.SelectMany(r => r.Questions).Where(q => q.QuestionNumber == count && q.Answer == Choices.StronglyAgree).Count();
                    var A = results.SelectMany(r => r.Questions).Where(q => q.QuestionNumber == count && q.Answer == Choices.Agree).Count();
                    var NS = results.SelectMany(r => r.Questions).Where(q => q.QuestionNumber == count && q.Answer == Choices.Neutral).Count();
                    var D = results.SelectMany(r => r.Questions).Where(q => q.QuestionNumber == count && q.Answer == Choices.Disagree).Count();
                    var SD = results.SelectMany(r => r.Questions).Where(q => q.QuestionNumber == count && q.Answer == Choices.StronglyDisagree).Count();
                    var S = results.SelectMany(r => r.Questions).Where(q => q.QuestionNumber == count && q.Answer == Choices.Skipped).Count();

                    ReplaceText($"[Q{count}SA]", $"{SA.ToString()} ({GetPercent((double)SA / report.ClassSize)})");
                    ReplaceText($"[Q{count}A]", $"{A.ToString()} ({GetPercent((double)A / report.ClassSize)})");
                    ReplaceText($"[Q{count}NS]", $"{NS.ToString()} ({GetPercent((double)NS / report.ClassSize)})");
                    ReplaceText($"[Q{count}D]", $"{D.ToString()} ({GetPercent((double)D / report.ClassSize)})");
                    ReplaceText($"[Q{count}SD]", $"{SD.ToString()} ({GetPercent((double)SD / report.ClassSize)})");
                    ReplaceText($"[Q{count}S]", $"{S.ToString()} ({GetPercent((double)S / report.ClassSize)})");
                }

                var goodthings = results.SelectMany(r => r.Questions).Where(q => q.QuestionNumber == 11).Select(q => q.Answer).ToList();
                var suggestions = results.SelectMany(r => r.Questions).Where(q => q.QuestionNumber == 12).Select(q => q.Answer).ToList();

                ReplaceText("[Q11Answer]", ConvertListToLines(goodthings));
                ReplaceText("[Q12Answer]", ConvertListToLines(suggestions));

                ReplaceText("[SA]", GetPercent(GetQuestionPerformance(results, Choices.StronglyAgree)));
                ReplaceText("[A]", GetPercent(GetQuestionPerformance(results, Choices.Agree)));
                ReplaceText("[NS]", GetPercent(GetQuestionPerformance(results, Choices.Neutral)));
                ReplaceText("[D]", GetPercent(GetQuestionPerformance(results, Choices.Disagree)));
                ReplaceText("[SD]", GetPercent(GetQuestionPerformance(results, Choices.StronglyDisagree)));
                ReplaceText("[S]", GetPercent(GetQuestionPerformance(results, Choices.Skipped)));

                ReplaceText("[Score]", GetPercent(GetTotalPerformance(results)));

                SetFlags(report, results);

                if (report.Flags.Count == 0)
                    ReplaceText("[Flag]", "No flag.");
                else
                    ReplaceText("[Flag]", ConvertListToLines(report.Flags));

                object filename = Path.Combine(locationForIndividualReports, $"{report.UnitCode} - {report.Lecturer}");
                doc.SaveAs2(filename, Word.WdSaveFormat.wdFormatPDF);
                doc.Close(Word.WdSaveOptions.wdDoNotSaveChanges);

                update?.Invoke(this, $"individual report for {report.UnitCode} - {report.Lecturer} has been generated.");
            }

            app.Quit();
        }

        private static void SetFlags(SurveyReport report, List<SurveyEntry> results)
        {
            if (results.Count() == 0)
                report.Flags.Add("No data to generate report.");
            if (report.ClassSize < 10)
                report.Flags.Add("The class size is less than 10 students.");
            if (((double)results.Count() / report.ClassSize) < 0.2)
                report.Flags.Add("The response rate is less than 20%.");
        }

        private void GenerateSummaryReport()
        {
            StringBuilder stringBuilder = new StringBuilder();
            IList<string> header = new List<string>
            {
                "Unit Code",
                "Unit Name",
                "Lecturer",
                "Overall Performance Indicator (%)",
                "Status",
                "Number of Flag",
                "Response Rate (%)",
                "Response",
                "Population",
                "Question 1",
                "Question 2",
                "Question 3",
                "Question 4",
                "Question 5",
                "Question 6",
                "Question 7",
                "Question 8",
                "Question 9",
                "Question 10"
            };

            stringBuilder.AppendLine(string.Join(",", header.ToArray()));

            IList<string> body = new List<string>();
            foreach (SurveyReport report in surveyReports)
            {
                var results = surveyEntries.Where(result => result.Id == report.Id).ToList();

                var totalPerformance = GetTotalPerformance(results);

                var status = "good";
                if (totalPerformance < 0.8)
                {
                    if (totalPerformance < 0.7)
                        status = "poor";
                    else
                        status = "alert";
                }
                else
                    status = "good";

                body.Clear();

                body.Add(report.UnitCode);
                body.Add($"\"{report.UnitName}\"");
                body.Add(report.Lecturer);
                body.Add(GetPercent(totalPerformance));
                body.Add(status);
                body.Add(report.Flags.Count().ToString());
                body.Add(GetPercent((double)results.Count() / report.ClassSize));
                body.Add(results.Count().ToString());
                body.Add(report.ClassSize.ToString());

                for (int count = 1; count <= 10; count++)
                {
                    var performance = results.SelectMany(r => r.Questions).Where(q => q.QuestionNumber == count && (q.Answer == Choices.StronglyAgree || q.Answer == Choices.Agree)).Count();
                    body.Add(performance.ToString());
                }

                stringBuilder.AppendLine(string.Join(",", body.ToArray()));
            }

            File.WriteAllText(Path.Combine(pathToExport, "SummaryReport.csv"), stringBuilder.ToString());

            update?.Invoke(this, $"summary report has been generated.");
        }

        private void ReplaceText(object placeholder, string content)
        {
            Word.Range range = doc.Content;
            range.Find.MatchWholeWord = true;
            range.Find.MatchCase = true;

            if (!range.Find.Execute(placeholder))
                return;

            range.Delete();

            range.Text = (string.IsNullOrEmpty(content)) ? string.Empty : content;
        }

        private string ConvertListToLines(IList<string> answers)
        {
            var validAnswer = answers.Where(ans => !string.IsNullOrEmpty(ans)).ToArray();

            if (validAnswer.Count() == 0)
                return string.Empty;

            return string.Join("\n", validAnswer);
        }

        private string GetPercent(double value)
        {
            return Math.Round(value * 100, 0).ToString() + "%";
        }

        private double GetQuestionPerformance(IList<SurveyEntry> results, string choice)
        {
            int totalAnswered = results.Count() * 10;
            int totalAnsweredThisChoice = 0;

            foreach (SurveyEntry result in results)
            {
                totalAnsweredThisChoice += result.Questions.Where(q => q.Answer == choice).Count();
            }

            return (double)totalAnsweredThisChoice / totalAnswered;
        }

        private double GetTotalPerformance(IList<SurveyEntry> results)
        {
            int totalAnswered = results.Count() * 10;
            int totalGoodAnswers = 0;

            foreach (SurveyEntry result in results)
            {
                totalGoodAnswers += result.Questions.Where(q => q.Answer == Choices.StronglyAgree || q.Answer == Choices.Agree).Count();
            }

            return (double)totalGoodAnswers / totalAnswered;
        }

        public void OnQuit()
        {
            try
            {
                doc.Close();
                app.Quit();
            }
            catch { }
        }
    }
}
