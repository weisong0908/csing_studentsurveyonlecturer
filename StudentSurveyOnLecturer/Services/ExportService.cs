﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StudentSurveyOnLecturer.Services
{
    public class ExportService
    {
        FolderBrowserDialog browserDialog;

        public string SetLocation()
        {
            browserDialog = new FolderBrowserDialog();

            if (browserDialog.ShowDialog() == DialogResult.OK)
                return browserDialog.SelectedPath;

            return string.Empty;
        }
    }
}
