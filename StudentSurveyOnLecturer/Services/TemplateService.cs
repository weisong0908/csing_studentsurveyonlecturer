﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows.Forms;

namespace StudentSurveyOnLecturer.Services
{
    public class TemplateService
    {
        private StringBuilder stringBuilder;
        IList<string> header;
        IList<string> body;

        public void CreateSurveyDataService()
        {
            stringBuilder = new StringBuilder();

            header = new List<string>
            {
                "Id",
                "Q1",
                "Q2",
                "Q3",
                "Q4",
                "Q5",
                "Q6",
                "Q7",
                "Q8",
                "Q9",
                "Q10",
                "Q11.1",
                "Q11.2",
                "Q11.3",
                "Q12.1",
                "Q12.2",
                "Q12.3"
            };

            body = new List<string>
            {
                "1",
                "StronglyAgree",
                "Agree",
                "Neutral",
                "Disagree",
                "StronglyDisagree",
                "",
                "StronglyAgree",
                "Agree",
                "Neutral",
                "Disagree",
                "Good point 1",
                "Good point 2",
                "Good point 3",
                "Suggestion 1",
                "Suggestion 2",
                "Suggestion 3"
            };

            stringBuilder.AppendLine(string.Join(",", header.ToArray()));
            stringBuilder.AppendLine(string.Join(",", body.ToArray()));

            SaveTemplate("SurveyDataTemplate");
        }        

        public void CreateReportDataTemplate()
        {
            stringBuilder = new StringBuilder();

            header = new List<string>
            {
                "Id",
                "Unit Code",
                "Unit Name",
                "Lecturer",
                "Class Size",
                "Study Term"
            };

            body = new List<string>
            {
                "1",
                "ABCD1234",
                "Unit Name 1",
                "example",
                "100",
                "2018 Trimester 1A"
            };

            stringBuilder.AppendLine(string.Join(",", header.ToArray()));
            stringBuilder.AppendLine(string.Join(",", body.ToArray()));

            SaveTemplate("ReportDataTemplate");
        }

        void SaveTemplate(string initialSuggestedFileName)
        {
            SaveFileDialog fileDialog = new SaveFileDialog();
            fileDialog.Filter = "csv files (*.csv)|*.csv";
            fileDialog.FileName = $"{initialSuggestedFileName}.csv";

            if (fileDialog.ShowDialog() == DialogResult.OK)
            {
                File.WriteAllText(fileDialog.FileName, stringBuilder.ToString());
            }
        }
    }
}
