﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Survey;
using Microsoft.VisualBasic.FileIO;

namespace StudentSurveyOnLecturer.Services
{
    public class DataProcessService2 
    {
        private readonly string surveyDataPath;
        private readonly string reportDataPath;
        private readonly string reportTemplatePath;
        private readonly string exportPath;
        private SurveyData surveyData;

        public DataProcessService2(string surveyDataPath, string reportDataPath, string reportTemplatePath, string exportPath)
        {
            this.surveyDataPath = surveyDataPath;
            this.reportDataPath = reportDataPath;
            this.reportTemplatePath = reportTemplatePath;
            this.exportPath = exportPath;

            surveyData = new SurveyData();

            // Set up survey questions
            var questions = new List<Question>();
            questions.Add(new Question("Q1", "The lecturer comes to class well prepared and organised."));
            questions.Add(new Question("Q2", "The lecturer explained clearly what was expected of me at the beginning of the study period."));
            questions.Add(new Question("Q3", "The lecturer uses the full allocated teaching time to help us to learn."));
            questions.Add(new Question("Q4", "The lecturer is approachable, helpful with students, and is available for consultation time."));
            questions.Add(new Question("Q5", "The lecturer explains ideas clearly and gives relevant examples which help me to understand."));
            questions.Add(new Question("Q6", "The lecturer is enthusiastic and knowledgeable about teaching the unit."));
            questions.Add(new Question("Q7", "The lecturer covers topics as stipulated in the unit outline."));
            questions.Add(new Question("Q8", "The lecturer explains the assessment methods for the unit clearly."));
            questions.Add(new Question("Q9", "The lecturer marks my assessment work and provides useful feedback on my performance."));
            questions.Add(new Question("Q10", "My overall rating of the lecturer is good."));
            questions.Add(new Question("Q11", "What are the 3 good things you observed about the lecturer?", true));
            questions.Add(new Question("Q12", "Suggestion for improvement", true));

            surveyData.Questions = questions;

            // Read survey answer
            using (var parser = new TextFieldParser(surveyDataPath))
            {
                parser.Delimiters = new string[] { "," };
                parser.HasFieldsEnclosedInQuotes = true;

                string[] headers = parser.ReadFields();

                while (!parser.EndOfData)
                {
                    string[] row = parser.ReadFields();

                    
                }
            }


            // Set up survey entries
            // Enter to the survey data
            // Generate individual report
            // generate summary report
        }


    }
}
