﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentSurveyOnLecturer.Models
{
    public class SurveyEntry
    {
        public int Id { get; set; }
        public IList<Question> Questions { get; set; }

        public SurveyEntry(string[] columns)
        {
            Questions = new List<Question>();
            IList<string> temporaryAnswers = new List<string>();

            Id = int.TryParse(columns[0], out int id) ? id : 0;

            for (int index = 1; index <= 10; index++)
            {
                var questionNumber = index;
                var category = SetCategory(questionNumber);

                Questions.Add(new Question(questionNumber, TranslateAnswer(columns[index]), category));
            }

            temporaryAnswers.Clear();
            for (int count = 11; count <= 13; count++)
            {
                if (!string.IsNullOrEmpty(columns[count]))
                    temporaryAnswers.Add(columns[count]);
            }
            Questions.Add(new Question(11, string.Join(". ", temporaryAnswers.ToList()), SetCategory(11)));

            temporaryAnswers.Clear();
            for (int count = 14; count <= 16; count++)
            {
                if (!string.IsNullOrEmpty(columns[count]))
                    temporaryAnswers.Add(columns[count]);
            }
            Questions.Add(new Question(12, string.Join(". ", temporaryAnswers.ToList()), SetCategory(12)));
        }

        private string TranslateAnswer(string rawAnswer)
        {
            switch (rawAnswer.ToLower())
            {
                case "strongly agree":
                case "stronglyagree":
                    return Choices.StronglyAgree;
                case "agree":
                    return Choices.Agree;
                case "not sure":
                case "neutral":
                    return Choices.Neutral;
                case "disagree":
                    return Choices.Disagree;
                case "strongly disagree":
                case "stronglydisagree":
                    return Choices.StronglyDisagree;
                default:
                    return Choices.Skipped;
            }
        }

        private string SetCategory(int questionNumber)
        {
            switch (questionNumber)
            {
                case 1:
                case 3:
                case 4:
                    return Categories.ServiceRendered;
                case 2:
                case 7:
                case 8:
                    return Categories.Effectiveness;
                case 5:
                case 6:
                case 9:
                    return Categories.Proficiency;
                case 11:
                    return Categories.Strength;
                case 12:
                    return Categories.ServiceRendered;
                case 10:
                    return Categories.Overall;
            }
            return string.Empty;
        }
    }
}
