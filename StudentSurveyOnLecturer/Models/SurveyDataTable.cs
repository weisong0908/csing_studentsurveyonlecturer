﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentSurveyOnLecturer.Models
{
    class SurveyDataTable
    {
        DataTable table;

        public SurveyDataTable()
        {
            table = new DataTable();
            DataColumn column1 = new DataColumn("Column 1");
            DataColumn column2 = new DataColumn("Column 2");

            table.Columns.AddRange(new DataColumn[] { column1, column2 });

            table.Rows.Add("data 1", "data 2");
        }
        
    }
}
