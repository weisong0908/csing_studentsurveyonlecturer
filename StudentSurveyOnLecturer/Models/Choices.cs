﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentSurveyOnLecturer.Models
{
    public static class Choices
    {
        public const string StronglyAgree = "StronglyAgree";
        public const string Agree = "Agree";
        public const string Neutral = "Neutral";
        public const string Disagree = "Disagree";
        public const string StronglyDisagree = "StronglyDisagree";
        public const string Skipped = "Skipped";
    }
}
