﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentSurveyOnLecturer.Models
{
    public static class Categories
    {
        public const string ServiceRendered = "Service Rendered";
        public const string Effectiveness = "Effectiveness";
        public const string Proficiency = "Proficiency";
        public const string Strength = "Strength";
        public const string Weakness = "Weakness";
        public const string Overall = "Overall";
    }
}
