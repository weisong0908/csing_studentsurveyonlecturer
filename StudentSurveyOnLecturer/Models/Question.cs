﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentSurveyOnLecturer.Models
{
    public class Question
    {
        public int QuestionNumber { get; set; }
        public string Answer { get; set; }
        public string Category { get; set; }

        public Question(int questionNumber, string answer, string category)
        {
            QuestionNumber = questionNumber;
            Answer = answer;
            Category = category;
        }
    }
}
