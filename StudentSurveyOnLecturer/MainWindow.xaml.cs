﻿using StudentSurveyOnLecturer.Services;
using System;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace StudentSurveyOnLecturer
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private SynchronizationContext synchronizationContext;

        private TemplateService templateService;
        private ImportService importService;
        private ExportService exportService;
        private DataProcessService dataProcessService;

        public MainWindow()
        {
            InitializeComponent();
            synchronizationContext = SynchronizationContext.Current;

            templateService = new TemplateService();
            importService = new ImportService();
            exportService = new ExportService();

            status.Text = "Idle";
        }

        private void GenerateSurveyDataTemplate(object sender, RoutedEventArgs e)
        {
            templateService.CreateSurveyDataService();
        }

        private void GenerateReportDataTemplate(object sender, RoutedEventArgs e)
        {
            templateService.CreateReportDataTemplate();
        }

        private void GetSurveyDataFile(object sender, RoutedEventArgs e)
        {
            surveyData.Text = importService.GetDataLocation();
        }

        private void GetReportDataFile(object sender, RoutedEventArgs e)
        {
            reportData.Text = importService.GetDataLocation();
        }

        private void GetReportTemplate(object sender, RoutedEventArgs e)
        {
            reportTemplate.Text = importService.GetTemplateLocation();
        }

        private void SetReportLocation(object sender, RoutedEventArgs e)
        {
            exportLocation.Text = exportService.SetLocation();
        }

        private async void ProcessSurveyResult(object sender, RoutedEventArgs e)
        {
            if (!System.IO.File.Exists(surveyData.Text))
            {
                MessageBox.Show("No survey data is imported");
                return;
            }

            if (!System.IO.File.Exists(reportData.Text))
            {
                MessageBox.Show("No report data is imported");
                return;
            }

            if (!System.IO.File.Exists(reportTemplate.Text))
            {
                MessageBox.Show("No report template is imported");
                return;
            }

            if (!System.IO.Directory.Exists(exportLocation.Text))
            {
                MessageBox.Show("No location is set for export");
                return;
            }

            progressBar.IsIndeterminate = true;

            dataProcessService = new DataProcessService(surveyData.Text, reportData.Text, reportTemplate.Text, exportLocation.Text);
            dataProcessService.update += OnUpdate;

            await Task.Run(() =>
            {
                dataProcessService.Start();
            });

            progressBar.IsIndeterminate = false;

            status.Text = "Completed.";
            MessageBox.Show("Completed");
        }

        public void OnUpdate(object sender, string updateMessage)
        {
            synchronizationContext.Post(new SendOrPostCallback(o =>
            {
                status.Text = $"Working: {o.ToString()}";

            }), updateMessage);
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            if (dataProcessService == null)
                return;
            dataProcessService.OnQuit();
        }
    }
}
